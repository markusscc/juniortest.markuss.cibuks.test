<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <title>Product List</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link src="js/jquery3.6.0.js"></link>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    
    </head>
    
    <body>
        
    <nav class="navbar navbar-expand-md navbar-dark bg-danger fixed-top">
        <div class="container">
            <a class="navbar-brand" href="index.php"></i>Product Add</a> 
        </div>
        
    </nav>

    <div class="container">
        <a href="index.php" class="btn btn-light mb-3"><< Go Back</a>
        <?php if (isset($_GET['status']) && $_GET['status'] == "created") : ?>
        <div class="alert alert-success" role="alert">
            <strong>Created</strong>
        </div>
        <?php endif ?>
        <?php if (isset($_GET['status']) && $_GET['status'] == "fail_create") : ?>
        <div class="alert alert-danger" role="alert">
            <strong>Fail Create</strong>
        </div>
        <?php endif ?>
        <!-- Create Form -->
            <div>
                <form action="add.php" method="post">
                    <div>
                        <div >
                            <label for="sku" class="col-form-label">SKU</label>
                            <input type="text" class="form-control" id="sku" name="sku" placeholder="SKU" required>
                        </div>
                        <div>
                            <label for="name" class="col-form-label">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="price" class="col-form-label">Price</label>
                            <input type="number" class="form-control" id="price" name="price" placeholder="Price" required>
                        </div>
                    </div>   

                    <div class="container">
                            <label>Type Switcher</label>
                                <fieldset>
                                    <select class="form-control" name="productType" id="productType" class="productType" onclick="display();">
                                        <option  value="typeswitcher" selected disabled hidden >Type Switcher</option> 
                                        <option  value="DVD">DVD-disc</option>             
                                        <option  value="Book">Book</option> 
                                        <option  value="Furniture">Furniture</option> 
                                    </select>

                                    <script>
                                        function display() {
                                            var x = document.getElementById('productType').value;
                                            
                                            if (x=="DVD") {
                                                document.getElementById('DVDdiscDetails').style.display="block";
                                                document.getElementById('BookDetails').style.display="none";
                                                document.getElementById('FurnitureDetails').style.display="none";
                                            } else if (x=="Book"){
                                                document.getElementById('DVDdiscDetails').style.display="none";
                                                document.getElementById('BookDetails').style.display="block";
                                                document.getElementById('FurnitureDetails').style.display="none";
                                            } 
                                            
                                            else if (x=="Furniture"){
                                                document.getElementById('DVDdiscDetails').style.display="none";
                                                document.getElementById('FurnitureDetails').style.display="block";
                                                document.getElementById('BookDetails').style.display="none";
                                            }
                                            
                                        }
                                    </script>
                            
                                <div  name="productType"  id="FurnitureDetails" style="display: none;">
                                        <label name="productType" class="col-form-label">Height (CM)</label>
                                        <input  class="form-control" stype="text" name="height"  placeholder="Height">

                                        <label name="productType" class="col-form-label">Width (CM)</label>
                                        <input class="form-control" type="text" name="width"  placeholder="Width">

                                        <label name="productType" class="col-form-label">Length (CM)</label>
                                        <input class="form-control" type="text" name="length"  placeholder="Length">

                                        <caption class="container">Please provide dimensions ir HxWxL format </caption>
                                </div>   


                                <div name="productType"  id="BookDetails" style="display: none;">
                                        <label name="productType" class="col-form-label">Weight (KG)</label>
                                        <input class="form-control" type="text" name="weight" placeholder="Weight"> 
                                        <p class="container">Please provide dimensions in KG format </p>                         
                                </div>
 
                                <div name="productType"  id="DVDdiscDetails" style="display: none;">   
                                            <label name="productType" class="col-form-label">Size (MB)</label>
                                            <input class="form-control"type="text" name="size" placeholder="Size" >   
                                        <p class="container">Please provide dimensions in MB format </p>
                                </div>    
                                
                                <nav class="navbar navbar-expand-md navbar-dark bg-danger fixed-top">
                                            <div class="container">
                                                <a class="navbar-brand" href="index.php"></i>Product Add</a> 
                                            </div>

                                            <div>
                                            <button type="submit" class="btn btn-success"><i class="fa fa-check-circle"></i> Save</button>
                                            </div>

                                            <div>                                               
                                            <button type="reset" method="get" action="index.php" class="btn btn-reset"> Cancel</button>
                                            </div>
                                </nav>
                              
                             
            </div>
        </div>




    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery3.6.0"></script>
    <script src="js/bootstrap.js"></script>

  </body>
</html>
