<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Product List</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link src="js/jquery3.6.0.js"></link>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    
<style>
    tr{

height:20px;
width: 60px; 
text-align:center; 

vertical-align: middle;

}
</style>



  </head>

  <body>
    <!-- Start Navabar-->
    <nav class="navbar navbar-expand-md navbar-dark bg-danger fixed-top">
        <div class="container">
            <a class="navbar-brand" href="index.php"></i>Product List</a>
            <a href="create.php" class="btn btn-success float-right mb-3"><i class="fa fa-plus"></i> Add New</a>       
        </div>
    </nav>

<?php 
// Include database connection
require("inc/db.php");

try {
    // Create sql statment
    $sql = "SELECT * FROM products";
    $result = $conn->query($sql);

} catch (Exception $e) {
    echo "Error " . $e->getMessage();
    exit();
}

?>
<?php include("inc/header.php") ?>
    <div class="container">
        <?php if (isset($_GET['status']) && $_GET['status'] == "deleted") : ?>
        <div class="alert alert-success" role="alert">
            <strong>Deleted</strong>
        </div>
        <?php endif ?>
        <?php if (isset($_GET['status']) && $_GET['status'] == "fail_delete") : ?>
        <div class="alert alert-danger" role="alert">
            <strong>Fail Delete</strong>
        </div>
        <?php endif ?>
        <!-- Table Product -->
                    
                        <table>
                            <tbody>
                            <?php if ($result->rowCount() > 0) : ?>
                                <?php foreach ($result as $product) : ?>
                        
                                <tr>
                                    <td><?= $product['sku'] ?></td>
                                </tr>
                                    <td><?= $product['name'] ?></td>
                                </tr>
                                <tr>
                                    <td>Price: $<?= number_format($product['price'], 2) ?></td>
                                </tr>
                                </tr>
                                    <td><?= $product['productType'] ?></td>
                                </tr>
                                <tr>
                                    <td>Height (CM):<?= $product['height'] ?></td>
                                </tr>
                                <tr>
                                    <td>Width (CM):<?= $product['width'] ?></td>
                                </tr>
                                <tr>
                                    <td>Length (CM):<?= $product['length'] ?></td>
                                </tr>
                                <tr>
                                    <td>Weigth (KG):<?= $product['weight'] ?></td>
                                </tr>
                                <tr>
                                    <td>Size(CM):<?= $product['size'] ?></td>
                                </tr>
                                
                                <tr>
                                    <td   href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-delete-<?= $product['id'] ?>"><i class="fa fa-trash"></i></td>
                                        <?php include("inc/modal.php") ?>
                                </tr>
                                
                            
                                <?php endforeach ?>
                            <?php endif ?>
                            </tbody>     
                        </table>

        </div>
      </div>
      <br>
 </div>
  </body>
</html>