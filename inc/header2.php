<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Basic PHP and MySQL application using PHP Data Object and Bootstarp 4">
    <meta name="author" content="Sok Kimsoeurn">

    <title>Product List</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
  </head>

  <body>
    <!-- Start Navabar-->
    <nav class="navbar navbar-expand-md navbar-dark bg-danger fixed-top" >
        <div class="container">
            <a class="navbar-brand" href="index.php"></i>Product Add</a>

            <div class="collapse navbar-collapse">
                    
            </div>
        </div>
    </nav>
  </body>
    <!-- End Navbar -->