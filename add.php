<?php 
require("inc/db.php");

if ($_POST) {
    $sku = ($_POST['sku']);
    $name    = trim($_POST['name']);
    $price   = (float) $_POST['price'];
    $productType = ($_POST['productType']);
    $height = ($_POST['height']);
    $width = ($_POST['width']);
    $length = ($_POST['length']);
    $weight= ($_POST['weight']);
    $size = ($_POST['size']);

    try {
        $sql = 'INSERT INTO products(sku, name, price, productType, height, width, length, weight, size) 
                VALUES(:sku, :name, :price, :productType, :height, :width, :length, :weight, :size)';

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(":sku", $sku);
        $stmt->bindParam(":name", $name);
        $stmt->bindParam(":price", $price);
        $stmt->bindParam(":productType", $productType);
        $stmt->bindParam(":height", $height);
        $stmt->bindParam(":width", $width);
        $stmt->bindParam(":length", $length);
        $stmt->bindParam(":weight", $weight);
        $stmt->bindParam(":size", $size);
        $stmt->execute();
        if ($stmt->rowCount()) {
            header("Location: create.php?status=created");
            exit();
        }
        header("Location: create.php?status=fail_create");
        exit();
    } catch (Exception $e) {
        echo "Error " . $e->getMessage();
        exit();
    }
} else {
    header("Location: create.php?status=fail_create");
    exit();
}
?>
